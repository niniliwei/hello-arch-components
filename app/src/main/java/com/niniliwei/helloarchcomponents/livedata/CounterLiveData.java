package com.niniliwei.helloarchcomponents.livedata;

import android.arch.lifecycle.LiveData;

/**
 * Created by liwei on 2017/10/9.
 */

public class CounterLiveData extends LiveData<Integer> {
    public CounterLiveData() {
        setValue(0);
    }

    public void increaseCounter() {
        setValue(getValue() + 1);
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onInactive() {
        super.onInactive();
    }
}
