package com.niniliwei.helloarchcomponents.livedata.transform;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

/**
 * Created by liwei on 2017/10/9.
 */

public class CalculationViewModel extends ViewModel {
    private final MutableLiveData<String> inputName = new MutableLiveData<>();
    private final LiveData<Integer> nameCounting = Transformations.map(inputName, String::length);

    public void setName(String name) {
        inputName.setValue(name);
    }

    public LiveData<Integer> getCount() {
        return nameCounting;
    }
}
