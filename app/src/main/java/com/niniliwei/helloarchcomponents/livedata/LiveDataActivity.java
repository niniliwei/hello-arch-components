package com.niniliwei.helloarchcomponents.livedata;

import android.os.Bundle;

import com.niniliwei.helloarchcomponents.MainActivity;

/**
 * Created by liwei on 2017/10/9.
 */

public class LiveDataActivity extends MainActivity {
    private CounterLiveData liveData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        liveData = new CounterLiveData();
        subscribeToLiveData();
    }

    private void subscribeToLiveData() {
        liveData.observe(this, value -> {
            counterView.setText(String.valueOf(value));
        });
    }

    @Override
    public void onFabClicked() {
        liveData.increaseCounter();
    }
}
