package com.niniliwei.helloarchcomponents.livedata.transform;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;

/**
 * Created by liwei on 2017/10/9.
 */

public class CustomTransformation {
    public static LiveData<String> customToString(LiveData<Integer> source) {
        MediatorLiveData<String> mediator = new MediatorLiveData<>();
        mediator.addSource(source, value -> mediator.setValue(String.valueOf(value)));
        return mediator;
    }
}
