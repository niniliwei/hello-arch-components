package com.niniliwei.helloarchcomponents.livedata.transform;

import android.arch.lifecycle.LiveData;

/**
 * Created by liwei on 2017/10/9.
 */

public class BackToStringLiveData extends LiveData<String> {
    LiveData<String> fromInt(final int value) {
        setValue(String.valueOf(value));
        return this;
    }
}
