package com.niniliwei.helloarchcomponents.livedata.transform;

import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.TextView;

import com.niniliwei.helloarchcomponents.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by liwei on 2017/10/9.
 */

public class TransformationActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.inputText)
    EditText input;
    @BindView(R.id.outputText)
    TextView output;

    private CalculationViewModel viewModel;
    private final BackToStringLiveData liveData = new BackToStringLiveData();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transformation);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        viewModel = ViewModelProviders.of(this).get(CalculationViewModel.class);
        Transformations.switchMap(viewModel.getCount(), liveData::fromInt)
                .observe(this, output::setText);
    }

    @OnClick(R.id.calculateButton)
    public void onCalculateRequested() {
        viewModel.setName(input.getText().toString());
    }
}
