package com.niniliwei.helloarchcomponents.livedatainviewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.niniliwei.helloarchcomponents.livedata.CounterLiveData;

/**
 * Created by liwei on 2017/10/9.
 */

public class CounterLiveViewModel extends ViewModel {

    private final CounterLiveData liveData = new CounterLiveData();

//    public CounterLiveViewModel(Application application) {
//        super(application);
//        liveData = new CounterLiveData();
//    }

    public LiveData<Integer> getCounter() {
        return liveData;
    }

    public void increaseCounter() {
        liveData.increaseCounter();
    }
}
