package com.niniliwei.helloarchcomponents.livedatainviewmodel;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;

import com.niniliwei.helloarchcomponents.MainActivity;
import com.niniliwei.helloarchcomponents.lifecycle.SimpleLifecycleObserver;

/**
 * Created by liwei on 2017/10/9.
 */

public class ViewModelLiveDataActivity extends MainActivity {
    private CounterLiveViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(CounterLiveViewModel.class);
        subscribeToLiveDataChanges();
        SimpleLifecycleObserver.bindInto(this);
    }

    private void subscribeToLiveDataChanges() {
        viewModel.getCounter().observe(this, value -> {
            counterView.setText(String.valueOf(value));
        });
    }

    @Override
    public void onFabClicked() {
        viewModel.increaseCounter();
    }
}
