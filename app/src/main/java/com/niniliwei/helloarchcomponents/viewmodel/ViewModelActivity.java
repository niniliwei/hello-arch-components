package com.niniliwei.helloarchcomponents.viewmodel;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;

import com.niniliwei.helloarchcomponents.MainActivity;

/**
 * Created by liwei on 2017/10/9.
 */

public class ViewModelActivity extends MainActivity {

    private CounterViewModel counterViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        counterViewModel = ViewModelProviders.of(this).get(CounterViewModel.class);
        updateCounterValue(counterViewModel.getCounter());
    }

    public void onFabClicked() {
        counterViewModel.increaseCounter();
        updateCounterValue(counterViewModel.getCounter());
    }

    private void updateCounterValue(int v) {
        counterView.setText(String.valueOf(v));
    }
}
