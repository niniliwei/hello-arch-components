package com.niniliwei.helloarchcomponents.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;

/**
 * Created by liwei on 2017/10/9.
 */

public class CounterViewModel extends AndroidViewModel {

    private int i = 0;

    public CounterViewModel(Application application) {
        super(application);
    }

    public int getCounter() {
        return i;
    }

    public void increaseCounter() {
        i++;
    }
}
